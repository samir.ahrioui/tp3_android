package fr.samir.ahrioui.tp3_android.controller;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import fr.samir.ahrioui.tp3_android.R;
import fr.samir.ahrioui.tp3_android.model.City;
import fr.samir.ahrioui.tp3_android.model.WeatherDbHelper;
import fr.samir.ahrioui.tp3_android.view.MyRecyclerViewAdapter;
import fr.samir.ahrioui.tp3_android.webservice.JSONResponseHandler;
import fr.samir.ahrioui.tp3_android.webservice.WebServiceUrl;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, MyRecyclerViewAdapter.ItemClickListener {

    public static final String SELECTED_CITY = "SELECTED_CITY";
    private static final int NEW_CITY_ACTIVITY_REQUEST_CODE = 42;
    private static final int CITY_ACTIVITY_REQUEST_CODE = 43;
    //private ListView mListView;
    private SwipeRefreshLayout mRefreshLayout;
    private WeatherDbHelper dbHelper;
    //private SimpleCursorAdapter cursorAdapter;
    private MyRecyclerViewAdapter adapter;
    private RecyclerView recyclerView;
    //private Cursor mCursor;
    private List<City> mCities;

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRefreshLayout = findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);
        // mListView = findViewById(R.id.list_cities);
        dbHelper = new WeatherDbHelper(this);
        dbHelper.populate();
        mCities = dbHelper.getAllCities();
       /* cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                mCursor,
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.
                        COLUMN_COUNTRY},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);
        mListView.setAdapter(cursorAdapter);*/


        // set up the RecyclerView
        recyclerView = findViewById(R.id.list_cities);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, mCities);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewCityActivity.class);
                startActivityForResult(intent, NEW_CITY_ACTIVITY_REQUEST_CODE);


            }
        });


    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(MainActivity.this, CityActivity.class);
        intent.putExtra(SELECTED_CITY, mCities.get(position));
        startActivityForResult(intent, CITY_ACTIVITY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (CITY_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City updatedCity = data.getParcelableExtra(MainActivity.SELECTED_CITY);
            dbHelper.updateCity(updatedCity);
            mCities = dbHelper.getAllCities();
            adapter.setData(mCities);
            adapter.notifyDataSetChanged();


            // adapter.notifyItemChanged((int)updatedCity.getId());
            // System.out.println("City :" + updatedCity.getName() + " Country :" + updatedCity.getCountry());
        }
        if (NEW_CITY_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
            City newCity = data.getParcelableExtra(NewCityActivity.BUNDLE_EXTRA_NEW_CITY);
            dbHelper.addCity(newCity);
            mCities = dbHelper.getAllCities();
            adapter.setData(mCities);
            adapter.notifyDataSetChanged();
            //adapter.notifyItemInserted((int)newCity.getId());
            //System.out.println("City :" + newCity.getName() + " Country :" + newCity.getCountry());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        if (MainActivity.isOnline(this))
            new UpdateCitiesWheather().execute();
        else {
            Snackbar.make(findViewById(R.id.refresh_layout), "Vérifiez votre connexion internet", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            mRefreshLayout.setRefreshing(false);
        }

    }

    class UpdateCitiesWheather extends AsyncTask<Object, Integer, List<City>> {
        private List<City> mCityList;

        @Override
        protected void onPreExecute() {
            // mCityList = dbHelper.getAllCities();
            super.onPreExecute();
        }

        @Override
        protected List<City> doInBackground(Object... objects) {
            for (City city : mCities) {
                weatherRequest(city);
                dbHelper.updateCity(city);
            }
            return mCityList;
        }


        @Override
        protected void onPostExecute(List<City> cities) {
            mCities = dbHelper.getAllCities();
            adapter.setData(mCities);
            adapter.notifyDataSetChanged();
            mRefreshLayout.setRefreshing(false);
            super.onPostExecute(cities);

        }

        private void weatherRequest(City city) {
            HttpURLConnection urlConnection = null;
            try {
                // prepare url
                URL urlToRequest = WebServiceUrl.build(city.getName(), city.getCountry());
                // send a GET request to the serve
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                // read data
                InputStream inputStream = urlConnection.getInputStream();
                JSONResponseHandler jsonHandler = new JSONResponseHandler(city);
                jsonHandler.readJsonStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        }
    }

    public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
        private MyRecyclerViewAdapter mAdapter;

        public SwipeToDeleteCallback(MyRecyclerViewAdapter adapter) {
            super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            mAdapter = adapter;
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            int position = viewHolder.getAdapterPosition();
            int idRemoveItem = mAdapter.deleteItem(position);
            dbHelper.deleteCity(idRemoveItem);

        }
    }
}
