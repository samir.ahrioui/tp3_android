package fr.samir.ahrioui.tp3_android.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.samir.ahrioui.tp3_android.R;
import fr.samir.ahrioui.tp3_android.model.City;


public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;
    public static final String BUNDLE_EXTRA_NEW_CITY = "BUNDLE_EXTRA_NEW_CITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = findViewById(R.id.editNewName);
        textCountry = findViewById(R.id.editNewCountry);

        final Button but = findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                City city = new City(textName.getText().toString(), textCountry.getText().toString());
                Intent intent = new Intent();
                intent.putExtra(BUNDLE_EXTRA_NEW_CITY, city);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

}
