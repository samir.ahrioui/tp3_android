package fr.samir.ahrioui.tp3_android.view;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import fr.samir.ahrioui.tp3_android.R;
import fr.samir.ahrioui.tp3_android.model.WeatherDbHelper;

import static fr.samir.ahrioui.tp3_android.controller.CityActivity.c;


public class CitiesCursorAdapter extends BaseCursorAdapter<CitiesCursorAdapter.CityViewHolder> {
    private static final String TAG = CitiesCursorAdapter.class.getSimpleName();

    public CitiesCursorAdapter(Context c) {
        super(null, c);
    }

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View formNameView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new CityViewHolder(formNameView);
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, Cursor cursor) {
        holder.CityNameTextView.setText(cursor.getString(cursor.getColumnIndex(WeatherDbHelper.COLUMN_CITY_NAME)));
        holder.textCountry.setText(cursor.getString(cursor.getColumnIndex(WeatherDbHelper.COLUMN_COUNTRY)));
        holder.textTemperature.setText(cursor.getString(cursor.getColumnIndex(WeatherDbHelper.COLUMN_TEMPERATURE)));

        String weatherIcon = cursor.getString(cursor.getColumnIndex(WeatherDbHelper.COLUMN_ICON));
        String weatherDescrption = cursor.getString(cursor.getColumnIndex(WeatherDbHelper.COLUMN_DESCRIPTION));
        if (weatherIcon != null && !weatherIcon.isEmpty()) {
            Log.d(TAG, "icon=" + "icon_" + weatherIcon);
            holder.imageWeatherCondition.setImageDrawable(c.getResources().getDrawable(c.getResources()
                    .getIdentifier("@drawable/" + "icon_" + weatherIcon, null, c.getPackageName())));
            holder.imageWeatherCondition.setContentDescription(weatherDescrption);
        }
    }

    @Override
    public void swapCursor(Cursor newCursor) {
        super.swapCursor(newCursor);
    }


    class CityViewHolder extends RecyclerView.ViewHolder {

        TextView CityNameTextView, textCountry, textTemperature;
        ImageView imageWeatherCondition;

        CityViewHolder(View itemView) {
            super(itemView);
            CityNameTextView = itemView.findViewById(R.id.cName);
            textCountry = itemView.findViewById(R.id.cCountry);
            textTemperature = itemView.findViewById(R.id.temperature);
            imageWeatherCondition = itemView.findViewById(R.id.imageViewRow);

        }
    }
}
