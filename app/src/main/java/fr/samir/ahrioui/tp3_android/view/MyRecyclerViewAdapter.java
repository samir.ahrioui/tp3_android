package fr.samir.ahrioui.tp3_android.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.samir.ahrioui.tp3_android.R;
import fr.samir.ahrioui.tp3_android.model.City;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<City> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    // data is passed into the constructor
    public MyRecyclerViewAdapter(Context context, List<City> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
    }
    public void setData(List<City> data){
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        City city = mData.get(position);
        holder.CityNameTextView.setText(city.getName());
        holder.textCountry.setText(city.getCountry());
        holder.textTemperature.setText(city.getTemperature());

        if (city.getIcon() != null && !city.getIcon().isEmpty()) {
            Log.d(this.getClass().getSimpleName(), "icon=" + "icon_" + city.getIcon());
            holder.imageWeatherCondition.setImageDrawable(mContext.getResources().getDrawable(mContext.getResources()
                    .getIdentifier("@drawable/" + "icon_" + city.getIcon(), null, mContext.getPackageName())));
            holder.imageWeatherCondition.setContentDescription(city.getDescription());
        }

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public int deleteItem(int position) {
        int mRecentlyDeletedItem = (int)mData.get(position).getId();
        mData.remove(position);
        notifyItemRemoved(position);
        return mRecentlyDeletedItem;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView CityNameTextView, textCountry, textTemperature;
        ImageView imageWeatherCondition;

        ViewHolder(View itemView) {
            super(itemView);
            CityNameTextView = itemView.findViewById(R.id.cName);
            textCountry = itemView.findViewById(R.id.cCountry);
            textTemperature = itemView.findViewById(R.id.temperature);
            imageWeatherCondition = itemView.findViewById(R.id.imageViewRow);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public City getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
